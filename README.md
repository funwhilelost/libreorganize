# Status

Branch | Pipeline | Coverage
--- | --- | ---
release | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/release/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/release) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/release/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/release)
master | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/master/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/master) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/master/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/master)

# About

- LibreOrganize is a web platform for local unions and progressive organizations.
- We are helping build popular power through democratic control of our data.

# Installation

## LibreOrganize

1. Clone this repository.

2. Create a virtual environment using `$ python3 -m venv venv`

3. Activate the virtual environment using `$ source venv/bin/activate`.

4. Install the dependencies using `$ pip install -r requirements.txt`.

5. Change the directory to `libreorganize` using `$ cd libreorganize/`.

6. Apply the migrations using `$ python manage.py migrate`.

7. Load the initial data using `$ python manage.py loaddata core/fixtures/initial_data.json`.

## Theme

*Follow these instructions only if you want to install a custom theme.*

1. Change the directory to `libreorganize` using `$ cd libreorganize/`.

2. Clone the theme repository using `$ git clone git@gitlab.com:libreorganize/libreorganize-themes.git theme/`.

3. Edit the file `settings.py` and apply your custom settings.

4. If applicable, edit the file `urls.py` and create URLs for your theme.


# Development

*Always activate the virtual environment before performing operations.*

- Run the server using `$ python manage.py runserver`.

- Stop the server by pressing `Ctrl-C`.

# Testing

*Always activate the virtual environment before performing operations.*

*Automated tests are supported only on Unix-like Operating Systems.*

1. Change the directory to `libreorganize` using `$ cd libreorganize/`.

2. Run Functional Tests using `$ source tests/automation/run_func_tests.sh`.

3. Run Unit Tests using `$ source tests/automation/run_unit_tests.sh`.

4. The code coverage will be printed after the execution of all Unit Tests.

# Deployment

*Not available yet.*

# SMTP Setup

```
SMTPServerDisconnected at /memberships/enroll/
please run connect() first
```

If you get the error above it means you didn't set up SMTP correctly. To set up SMTP follow the following steps...

1. Go to the .env file 

2. Ensure the following fields are filled out
```
    LO_EMAIL_HOST=""
    LO_EMAIL_HOST_USER=""
    LO_EMAIL_HOST_PASSWORD=""
```
**Ouput Email to Terminal**: ```EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'```
