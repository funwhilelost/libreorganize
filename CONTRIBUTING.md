# How to Contribute

- Every feature or bug fix shall be addressed in a different branch / fork.

- Merge Requests shall be the only way to get code into the `dev` branch.

- The issue number (if applicable) shall be referenced when submitting a Merge
  Request.

- The applications that are included in LibreOrganize V2 shall reside in the
  main repository <https://gitlab.com/libreorganize/libreorganize/> and shall
  not be available in an extraneous repository.

- You shall cede your work's copyright to Stefan-Ionut Barbu (Tarabuta) when
  contributing to this project.

# Code Guidelines

- The Python Formatter `black` with the maximum line length set to 120
  characters shall be used to format `.py` files.  
  *Note: This doesn't mean that files cannot exceed 120 characters.*

- All variables and default values shall be in English. Translation shall be
  provided with localization.

- All templates shall inherit from the `base.html` template. Additional
  base templates shall not be created unless absolutely necessary or with a
  visible improvement to code readability.

- All templates shall match the style and design of the current templates. This
  includes, but is not limited to, icons, padding, margins, fonts, titles, and
  tables.

- No `admin.py` file shall exist in any application. The Django admin
  interface shall remain disabled.

- Applications shall be self-contained. The templates and static files shall be
  stored in each application's folder.

- Django messages shall be preferred over displaying a view. Messages shall only
  be created in `views.py`.

- An application's name shall be a single, plural word. E.g. accounts, events

- The application templates shall be named using a single, plural word, when
  possible. They shall not include the application name.

- The application templates shall be placed in the `templates/application_name/`
  directory. Additional directories shall be created when deemed necessary to
  respect the rule above.

- Applications shall contain at least these files: `apps.py`, `forms.py`,
  `models.py`, `views.py`, and `urls.py`. Extra files can be added shall it be
  necessary.

# Final Note

*No Merge Request shall be accepted if it does not adhere to the rules mentioned
above.*
