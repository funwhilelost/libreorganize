from core.settings.common import *

DEBUG = False

SECRET_KEY = os.environ['SECRET_KEY']

ALLOWED_HOSTS = [h.strip() for h in os.environ.get("ALLOWED_HOSTS", "").split(",")]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": os.environ["LO_DB_NAME"],
        "USER": os.environ["LO_DB_USER"],
        "PASSWORD": os.environ["LO_DB_PASSWORD"],
        "HOST": os.environ.get("LO_DB_HOST", "localhost"),
        "PORT": os.environ.get("LO_DB_PORT", 5432),
    }
}

MEDIA_ROOT = os.environ.get("MEDIA_ROOT", os.path.join(BASE_DIR, "media"))
STATIC_ROOT = os.environ.get("STATIC_ROOT")


