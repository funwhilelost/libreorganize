#!/bin/bash

set -e

echo -e "\033[1;31mRunning Unit Tests with Coverage...\033[0m"
coverage run --source='.' --omit='*/core/*','local_settings.py','local_urls.py','manage.py','*/tests/*','*/migrations/*' manage.py test

coverage report -m
coverage erase
