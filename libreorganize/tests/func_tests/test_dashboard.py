from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.support.ui import WebDriverWait
from apps.accounts.models import Account
from utils import setup_browser, setup_account, force_login


class DashboardTest(StaticLiveServerTestCase):
    fixtures = ["core/fixtures/initial_data.json"]

    def setUp(self):
        self.browser = setup_browser()
        self.account = setup_account()
        force_login(self.account, self.browser, self.live_server_url)

    def tearDown(self):
        self.browser.quit()

    def test_dashboard(self):
        # Stefan wants to access some of the apps LibreOrganize offers.
        # He is already logged in, so he sees a dashboard button in the
        # upper right corner.
        self.browser.get(self.live_server_url + "/")
        self.browser.find_element_by_id("open-dashboard").click()

        # He sees a greeting message and a few buttons that link to
        # different apps.
        import time

        time.sleep(1)
        assert "Welcome to the Dashboard, Selenium-Test!" in self.browser.find_element_by_id("dashboard").text
