from django.utils.timezone import now

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import Account

class Membership(models.Model):
    uid = models.AutoField(primary_key=True)
    kind = models.CharField(max_length=16, choices=settings.MEMBERSHIP_TYPES, default="normal")
    start_date = models.DateField(help_text=_("Must be formatted as YYYY-MM-DD"))
    end_date = models.DateField(default=now, blank=True, help_text=_("Must be formatted as YYYY-MM-DD"))
    membership_period = models.CharField(max_length=16, choices=settings.MEMBERSHIP_PERIOD, default="one_year")
    payment_method = models.CharField(max_length=8, choices=settings.PAYMENT_METHODS, default="cash")
    member = models.OneToOneField(
        Account,
        on_delete=models.CASCADE,
        error_messages={"unique": _("There is already a membership associated with this account.")},
    )
    is_active = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_memberships", "List memberships"),
            ("create_memberships", "Create memberships"),
            ("edit_memberships", "Edit memberships"),
            ("delete_memberships", "Delete memberships"),
        )

class MembershipLog(models.Model):
    uid = models.AutoField(primary_key=True)
    member = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)
    kind = models.CharField(max_length=16, choices=settings.MEMBERSHIP_TYPES, default="normal")
    renewal_period_start = models.DateField(help_text=_("Must be formatted as YYYY-MM-DD"))
    renewal_period_end = models.DateField(default=now, blank=True, help_text=_("Must be formatted as YYYY-MM-DD"))
