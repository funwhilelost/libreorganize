from django.core.management.base import BaseCommand, CommandError
import datetime
from apps.memberships.models import Membership, MembershipLog

class Command(BaseCommand):
    help = 'Sets Membership is_active to false if end_date has passed'

    def handle(self, *args, **options):
        for membership in Membership.objects.all():
            if membership.end_date < datetime.date.today():
                if membership.is_active != False:
                    membership.is_active = False
                    membership.save()
                    self.stdout.write(self.style.SUCCESS(f"Membership {membership.uid} expired."))
                    MembershipLog(member=membership.member, kind=membership.kind, renewal_period_start=membership.start_date, renewal_period_end=membership.end_date).save()