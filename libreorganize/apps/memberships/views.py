import datetime

from django.conf import settings
from django.views import View
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.utils.translation import gettext_lazy as _
from django.views.generic.base import ContextMixin

from dateutil.relativedelta import relativedelta

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin
from apps.memberships.models import Membership, MembershipLog
from apps.memberships.forms import EnrollForm, CreateForm, EditForm


class ListView(AccessRestrictedMixin, View):
    permissions = ("memberships.list_memberships",)

    def get(self, request):
        memberships = Membership.objects.all()
        return render(request=request, template_name="memberships/list.html", context={"memberships": memberships})


class CreateView(AccessRestrictedMixin, NextPageMixin, ContextMixin, View):
    permissions = ("memberships.create_memberships",)

    def get(self, request):
        form = CreateForm()
        return render(request=request, template_name="memberships/create.html", context=self.get_context_data(form=form))

    def post(self, request):
        form = CreateForm(request.POST)
        if form.is_valid():
            membership = form.save(commit=False)
            membership.payment_method = "cash"
            membership.is_active = False
            membership.save()
            messages.add_message(request, messages.SUCCESS, _("The membership has been successfully created."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="memberships/create.html", context=self.get_context_data(form=form))


class EnrollView(AccessRestrictedMixin, NextPageMixin, ContextMixin, View):
    def get(self, request):
        if hasattr(request.user, "membership"):
            messages.add_message(request, messages.ERROR, _("You are already a member!"))
            return HttpResponseRedirect(self.next)
        form = EnrollForm()
        return render(request=request, template_name="memberships/enroll.html", context=self.get_context_data(form=form))

    def post(self, request):
        if hasattr(request.user, "membership"):
            messages.add_message(request, messages.ERROR, _("You are already a member!"))
            return HttpResponseRedirect(self.next)
        form = EnrollForm(request.POST)
        if form.is_valid():
            membership = form.save(commit=False)
            membership.start_date = datetime.date.today()
            membership.member = request.user
            membership.save()
            #if membership.payment_method == "cash":
            url = request.build_absolute_uri(f"/memberships/{membership.uid}/")
            html = render_to_string(
                "email.html",
                {
                    "url": url,
                    "message": _("A member who is paying by cash has joined! Login to activate his membership. Edit Membership and make the Account Active once you have recieved the money."),
                    "button": "View Membership",
                },
            )
            text = strip_tags(html).replace("View Membership", url)

            send_mail(
                subject=_("New Member | %s ") % settings.SITE_NAME,
                message=text,
                html_message=html,
                from_email=settings.EMAIL_HOST_USER,
                recipient_list=(settings.NOTIFICATIONS_EMAIL,),
            )
            messages.add_message(
                request,
                messages.SUCCESS,
                _(
                    f"Congratulations, {request.user.first_name}! You are almost a member of {settings.SITE_NAME}. An administrator will activate your membership soon."
                ),
            )
            #else:
            #    membership.is_active = True
            #    membership.save()
            #    messages.add_message(
            #        request,
            #        messages.SUCCESS,
            #        _(f"Congratulations, {request.user.first_name}! You are now a member of {settings.SITE_NAME}."),
            #    )
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="memberships/enroll.html", context=self.get_context_data(form=form))


class DetailView(AccessRestrictedMixin, AccessModelMixin, View):
    permissions = ("memberships.view_memberships",)
    personal = True
    model = Membership

    def get(self, request):
        return render(request=request, template_name="memberships/detail.html", context={"membership": self.membership})

class RenewalListView(AccessRestrictedMixin, View):
    permissions = ("memberships.view_memberships",)

    def get(self, request):
        memberships = MembershipLog.objects.all()
        return render(request=request, template_name="memberships/renewal_list.html", context={"memberships": memberships})

class EditView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, ContextMixin, View):
    permissions = ("memberships.edit_memberships",)
    model = Membership

    def get(self, request):
        form = EditForm(instance=self.membership)
        return render(request=request, template_name="memberships/edit.html", context=self.get_context_data(form=form))

    def post(self, request):
        form = EditForm(request.POST, instance=self.membership)
        if form.is_valid():
            if form.cleaned_data["membership_period"] == "one_year":
                self.membership.end_date = self.membership.start_date + relativedelta(years=1)
            elif form.cleaned_data["membership_period"] == "one_month":
                self.membership.end_date = self.membership.start_date + relativedelta(months=1)
            form.save()
            messages.add_message(request, messages.SUCCESS, _("The membership has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="memberships/edit.html", context=self.get_context_data(form=form))


class DeleteView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, ContextMixin, View):
    permissions = ("memberships.delete_memberships",)
    personal = True
    model = Membership

    def get(self, request):
        return render(request=request, template_name="memberships/delete.html", context=self.get_context_data())

    def post(self, request):
        self.membership.delete()
        messages.add_message(request, messages.SUCCESS, _("The membership has been deleted."))
        return HttpResponseRedirect(self.next)
