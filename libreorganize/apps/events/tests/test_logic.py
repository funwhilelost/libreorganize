from django.test import TestCase
from apps.accounts.models import Account
from apps.events.models import Event


class TestCalandarView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/calendar/")
        self.assertTemplateUsed(response, template_name="events/calendar.html")
    
    def test_months(self):
        """Check to make sure user can change months"""
        response = self.client.get("/events/calendar/?month=2020-4")
        self.assertContains(response, "April 2020")
        response = self.client.get("/events/calendar/?month=2020-5")
        self.assertContains(response, "May 2020")

class TestListView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/")
        self.assertTemplateUsed(response, template_name="events/list.html")

class TestCreateView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/create/")
        self.assertTemplateUsed(response, template_name="events/create.html")
    
    def test_end_date_before(self):
        """Check end date before start date (date)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-02-01 9:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "1",
                
            },
            follow=True,
        )
        self.assertContains(response, "The start date must be less than the end date.")
    
    def test_max_part_less_than_neg_one(self):
        """Max Participants Less than -1"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 13:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-2",
            },
            follow=True,
        )
        self.assertContains(response, "The maximum number of participants must be higher than -1.")

    def test_successful_weekly_event(self):
        """Check successful weekly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
        response = self.client.get("/events/calendar/?month=2020-3")
        self.assertContains(response, "Test")

    def test_successful_monthly_event(self):
        """Check successful montly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "monthly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")

    def test_successful_yearly_event(self):
        """Check successful yearly event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")
    
    def test_weekly_event(self):
        """Event is longer than one week and is marked as weekly"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-09 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "weekly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event must be shorter than 7 days if it reoccurs weekly.")

    def test_monthly_event(self):
        """Event is longer than one month and is marked as monthy"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "monthly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event must be shorter than 28 days if it reoccurs monthly.")

    def test_yearly_event(self):
        """Event is longer than one year and is marked as yearly"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "2",
            },
            follow=True,
        )
        self.assertContains(response, "The event must be shorter than 365 days if it reoccurs yearly.")

    def test_occurrences_bigger_than(self):
        """Occurrences is larger than 12"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "100",
            },
            follow=True,
        )
        self.assertContains(response, "The number of occurrences must be between 1 and 12.")

    def test_occurrences_smaller_than(self):
        """Occurrences is smaller than 1"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "0",
            },
            follow=True,
        )
        self.assertContains(response, "The number of occurrences must be between 1 and 12.")

    def test_occurrences(self):
        """Occurrences successful occurences"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2021-04-08 12:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "frequency": "yearly",
                "occurrences": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully created.")

class TestDetailView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/")
        self.assertTemplateUsed(response, template_name="events/detail.html")
    
    def test_event_does_not_exist(self):
        """Check if Event Exists"""
        response = self.client.get("/events/100/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")
    
    def test_event_detail_page(self):
        """Bring to Detail Page"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/", follow=True)
        self.assertContains(response, "Start Date")
    
    def test_private_event(self):
        """Test private event"""
        response = self.client.get("/events/1/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.assertRedirects(response, "/accounts/login/?next=/events/1/")


class TestEditView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/edit/")
        self.assertTemplateUsed(response, template_name="events/edit.html")
        response = self.client.post("/events/1/edit/")
        self.assertTemplateUsed(response, template_name="events/edit.html")
    
    def test_event_does_not_exist_edit(self):
        """Check if Event Exists Edit"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/edit/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")
    
    def test_event_does_exist(self):
        """Event Exists bring to details"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/edit/", follow=True)
        self.assertContains(response, "Start Date")

    def test_edit_event(self):
        """edit event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/1/edit/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 14:45",
                "end_date": "2020-03-01 15:45",
                "location": "1234 Washington Blvd",
                "max_participants": "-1",
                "occurrences": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully edited.")
        self.assertRedirects(response, "/")


class TestDeleteView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/delete/")
        self.assertTemplateUsed(response, template_name="events/delete.html")

    def test_incorrect_permissions_delete(self):
        """Incorrect Permissions to Delete"""
        self.client.force_login(self.user)
        response = self.client.get("/events/1/delete/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.assertRedirects(response, "/")
    
    def test_event_does_not_exist_delete(self):
        """Check if Event Exists Delete"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/delete/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")
    
    def test_delete(self):
        """Check delete"""
        self.client.force_login(self.superuser)
        response = self.client.post("/events/1/delete/", follow=True)
        self.assertContains(response, "The event has been deleted.")

class TestCheckView(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_event_does_not_exist_check(self):
        """Check if Event Exists Check"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/check/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_check_in(self):
        """Check in & out"""
        self.client.force_login(self.user)
        response = self.client.get("/events/1/check/", follow=True)
        self.assertRedirects(response, "/events/1/")
        self.assertContains(response, "You checked in.")
        response = self.client.get("/events/1/check/", follow=True)
        self.assertRedirects(response, "/events/1/")
        self.assertContains(response, "You have already checked in!")

    def test_max_participant(self):
        """No more participants allowed"""
        self.client.force_login(self.user)
        response = self.client.get("/events/2/check/", follow=True)
        self.assertRedirects(response, "/events/2/")
        self.assertContains(response, "You checked in.")
        self.client.force_login(self.superuser)
        response = self.client.get("/events/2/check/", follow=True)
        self.assertContains(response, "There are no more empty seats for this event!")

class TestSubscribeUtil(TestCase):
    fixtures = [
        "core/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Setup accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_event_subscribe(self):
        """Test download subscribe"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/subscribe", follow=True)

