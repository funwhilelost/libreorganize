from django.views import View
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.views import redirect_to_login
from django.views.generic.base import ContextMixin

from dateutil.relativedelta import relativedelta

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin
from apps.events.models import Event
from apps.events.forms import EventForm
from apps.events.utils import Calendar, get_date, prev_month, next_month


class CalendarView(View):
    def get(self, request, *args, **kwargs):
        date = get_date(self.request.GET.get("month", None))
        calendar = Calendar(date.year, date.month)

        context = {
            "calendar": calendar.formatmonth(withyear=True, privateevents=request.user.is_authenticated),
            "prev_month": prev_month(date),
            "next_month": next_month(date),
            "date": date
        }

        return render(request=request, template_name="events/calendar.html", context=context)


class ListView(AccessRestrictedMixin, View):
    permissions = ("events.list_events",)

    def get(self, request):
        events = Event.objects.all()
        return render(request=request, template_name="events/list.html", context={"events": events})


class CreateView(AccessRestrictedMixin, NextPageMixin, ContextMixin, View):
    permissions = ("events.create_events",)

    def get(self, request):
        form = EventForm()
        return render(request=request, template_name="events/create.html", context=self.get_context_data(form=form))

    def post(self, request):
        form = EventForm(request.POST)
        if form.is_valid():
            for occurrence in range(form.cleaned_data["occurrences"]):
                event = Event()
                for key, value in form.cleaned_data.items():
                    setattr(event, key, value)
                if form.cleaned_data["frequency"] == "weekly":
                    event.start_date = form.cleaned_data["start_date"] + relativedelta(weeks=occurrence)
                    event.end_date = form.cleaned_data["end_date"] + relativedelta(weeks=occurrence)
                elif form.cleaned_data["frequency"] == "monthly":
                    event.start_date = form.cleaned_data["start_date"] + relativedelta(months=occurrence)
                    event.end_date = form.cleaned_data["end_date"] + relativedelta(months=occurrence)
                elif form.cleaned_data["frequency"] == "yearly":
                    event.start_date = form.cleaned_data["start_date"] + relativedelta(years=occurrence)
                    event.end_date = form.cleaned_data["end_date"] + relativedelta(years=occurrence)
                event.save()
            messages.add_message(request, messages.SUCCESS, _("The event has been successfully created."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="events/create.html", context=self.get_context_data(form=form))


class DetailView(AccessModelMixin, View):
    model = Event

    def get(self, request):
        if not request.user.is_authenticated and self.event.is_private == True:
            messages.add_message(request, messages.SUCCESS, _("You don't have the required permissions."))
            return redirect_to_login(request.get_full_path(), "/accounts/login/", "next")
        else:
            return render(request=request, template_name="events/detail.html", context={"event": self.event})


class EditView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, ContextMixin, View):
    permissions = ("events.edit_events",)
    model = Event

    def get(self, request):
        form = EventForm(instance=self.event)
        return render(request=request, template_name="events/edit.html", context=self.get_context_data(form=form))

    def post(self, request):
        form = EventForm(request.POST, request.FILES, instance=self.event)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("The event has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="events/edit.html", context=self.get_context_data(form=form))


class DeleteView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, ContextMixin, View):
    permissions = ("events.delete_events",)
    model = Event

    def get(self, request):
        return render(request=request, template_name="events/delete.html", context=self.get_context_data())

    def post(self, request):
        self.event.delete()
        messages.add_message(request, messages.SUCCESS, _("The event has been deleted."))
        return HttpResponseRedirect(self.next)


class CheckView(AccessRestrictedMixin, AccessModelMixin, View):
    model = Event

    def get(self, request):
        if request.user in self.event.participants.all():
            messages.add_message(request, messages.WARNING, _("You have already checked in!"))
        else:
            if self.event.participants.count() >= self.event.max_participants and self.event.max_participants != -1:
                messages.add_message(request, messages.ERROR, _("There are no more empty seats for this event!"))
            else:
                self.event.participants.add(request.user)
                messages.add_message(request, messages.SUCCESS, _("You checked in."))
        return HttpResponseRedirect(reverse("events:detail", args={self.event.uid}))
