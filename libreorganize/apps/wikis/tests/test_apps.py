from django.test import TestCase
from apps.wikis.apps import WikisConfig


class WikisAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(WikisConfig.name, "apps.wikis")
