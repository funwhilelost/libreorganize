from django.db import models

from apps.accounts.models import Account


class Wiki(models.Model):
    uid = models.AutoField(primary_key=True)
    url = models.CharField(max_length=64, unique=True)
    title = models.CharField(max_length=64)
    content = models.TextField()

    class Meta:
        default_permissions = ()
        permissions = (
            ("view_wikis", "View wikis"),
            ("create_wikis", "Create wikis"),
            ("edit_wikis", "Edit wikis"),
            ("delete_wikis", "Delete wikis"),
        )

    def save(self, *args, **kwargs):
        self.url = self.title.replace(" ", "-").lower()
        super().save(*args, **kwargs)


class Log(models.Model):
    uid = models.AutoField(primary_key=True)
    wiki = models.ForeignKey(Wiki, on_delete=models.SET_NULL, null=True)
    content = models.TextField()
    author = models.ForeignKey(Account, on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(auto_now_add=True)
    action = models.CharField(max_length=8)

    class Meta:
        default_permissions = ()
