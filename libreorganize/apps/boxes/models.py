from django.db import models
from django.utils.translation import gettext_lazy as _


class Box(models.Model):
    uid = models.AutoField(primary_key=True)
    content = models.TextField()

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_boxes", "List boxes"),
            ("edit_boxes", "Edit boxes"),
        )
    
