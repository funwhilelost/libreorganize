FROM python:3.7-slim
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y -qq gettext libgettextpo-dev cron git

RUN mkdir /code
WORKDIR /code
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./libreorganize/ .

# Note - this does not scale beyond one image, it runs in _every image_
# COPY crontab /etc/cron.d/libreorganize
RUN echo "0 0 * * * cd /code && python manage.py update_membership_status " >> /etc/cron.d/libreorganize
RUN chmod 0644 /etc/cron.d/libreorganize
RUN crontab /etc/cron.d/libreorganize
